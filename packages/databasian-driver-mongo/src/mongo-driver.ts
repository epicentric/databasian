import {DriverInterface, DriverOptions, InsertDocument, DocumentInterface, createChangeObservable} from '@epicentric/databasian';
import {Observable, Subject} from 'rxjs';
import {Binary, Collection, Db, MongoClient} from 'mongodb';
import {Deferred} from '@epicentric/utilbelt';
import {cloneDeepWith} from 'lodash-es';
import {isBinaryId} from '@epicentric/binary-id';

export interface MongoDriverOptions
{
    address: string;
    database: string;
}

export class MongoDriver implements DriverInterface
{
    private readonly mongoClient: MongoClient;
    private readonly change: Subject<string> = new Subject;
    private readonly databaseName: string;
    private mongoDatabase!: Db;
    private options!: DriverOptions;
    private collections: Map<string, Collection> = new Map;
    
    public constructor(options: MongoDriverOptions)
    {
        this.databaseName = options.database;
        this.mongoClient = new MongoClient(options.address, {
            useNewUrlParser: true,
            // TODO: use this
            // promoteBuffers: true,
            
            // loggerLevel: 'debug'
        });
    }
    
    public async initialize(options: DriverOptions): Promise<void>
    {
        this.options = options;
        
        await this.mongoClient.connect();
        this.mongoDatabase = this.mongoClient.db(this.databaseName);

        for (const [name, config] of Object.entries(this.options.collectionConfigs || {}))
        {
            const collection = await this.mongoDatabase.createCollection(name);

            if (config.indexes)
            {
                for (const [indexName, indexSpec] of Object.entries(config.indexes))
                {
                    await collection.createIndex(indexSpec, { name: indexName });
                }
            }
        }
    }
    
    public find<T extends DocumentInterface>(collectionName: string, query?: object, sort?: { [p: string]: 1 | -1 }, limit?: number): Observable<T[]>
    {
        query = this.preProcessObject(query);
        
        return createChangeObservable(this.change, collectionName, this.options.changeThrottle,
            () => this.findSnapshot<T>(collectionName, query, sort, limit).then(this.postProcessObject)
        );
    }

    public async findOneAndRemove<T extends DocumentInterface>(collectionName: string, filter: object): Promise<T | null>
    {
        filter = this.preProcessObject(filter);
        
        const collection = await this.getCollection(collectionName);
        
        const result = await collection.findOneAndDelete(filter);
        this.triggerChange(collectionName);
        
        return this.postProcessObject(result.value || null);
    }

    public async removeMany<T extends DocumentInterface>(collectionName: string, filter: object): Promise<T[]>
    {
        filter = this.preProcessObject(filter);
        
        const collection = await this.getCollection(collectionName);

        const toBeDeleted = await collection.find(filter).toArray();
        await collection.deleteMany(filter);
        this.triggerChange(collectionName);

        return this.postProcessObject(toBeDeleted);
    }

    public async findOneAndUpdate<T extends DocumentInterface>(collectionName: string, filter: object, update: object): Promise<T | null>
    {
        filter = this.preProcessObject(filter);
        update = this.preProcessObject(update);
        
        const collection = await this.getCollection(collectionName);
        
        const result = await collection.findOneAndUpdate(filter, update, { returnOriginal: false });
        this.triggerChange(collectionName);
        
        return this.postProcessObject(result.value || null);
    }

    public async findSnapshot<T extends DocumentInterface>(collectionName: string, query?: object, sort?: { [p: string]: 1 | -1 }, limit?: number): Promise<T[]>
    {
        query = this.preProcessObject(query);
        
        const collection = await this.getCollection(collectionName);

        // Not really deprecated, bad type definition
        // noinspection JSDeprecatedSymbols
        return collection.find(query || {}, {
            sort,
            limit
        }).map(document => this.postProcessObject(document)).toArray();
    }

    public async insertMany<T extends DocumentInterface>(collectionName: string, documents: InsertDocument<T>[]): Promise<T[]>
    {
        documents = this.preProcessObject(documents);
        
        const collection = await this.getCollection(collectionName);
        
        const result = await collection.insertMany(documents);
        this.triggerChange(collectionName);
        
        return this.postProcessObject(result.ops);
    }

    public async updateMany<T extends DocumentInterface>(collectionName: string, query: object, update: object): Promise<void>
    {
        query = this.preProcessObject(query);
        update = this.preProcessObject(update);
        
        const collection = await this.getCollection(collectionName);
        
        const result = await collection.updateMany(query, update);
        this.triggerChange(collectionName);
        
        // TODO: Remove this if checked and use ops if exists
        console.log('check if ops exists', result);
    }

    public async getCollection(name: string): Promise<Collection>
    {
        let collection = this.collections.get(name);
        
        if (!collection)
        {
            const deferred = new Deferred<Collection>();

            this.mongoDatabase.collection(name, { strict: true }, (error, collection) => {
                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(collection);
            });
            
            collection = await deferred.promise;
            this.collections.set(name, collection);
        }
        
        return collection;
    }
    
    private triggerChange(collectionName: string): void
    {
        this.change.next(collectionName);
    }
    
    private preProcessObject<T extends object|undefined>(obj: T): T
    {
        // See:
        // http://mongodb.github.io/node-mongodb-native/3.1/api/Binary.html
        // https://stackoverflow.com/questions/34586432/understanding-binary-subtypes-in-bson-what-is-a-function-x01-and-what-poss
        return cloneDeepWith(obj, value => isBinaryId(value)
            ? new Binary(Buffer.from(value.buffer), Binary.SUBTYPE_USER_DEFINED)
            : undefined
        );
    }
    
    private postProcessObject<T extends object|undefined>(obj: T): T
    {
        return cloneDeepWith(obj, value => {
            if (!(value instanceof Binary) || value.sub_type !== Binary.SUBTYPE_USER_DEFINED)
                return undefined;
            
            // We need to a copy of the data
            const bufferCopy = Buffer.alloc(value.buffer.length);
            value.buffer.copy(bufferCopy);
            return new DataView(bufferCopy.buffer);
        });
    }
}