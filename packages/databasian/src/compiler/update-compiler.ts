import {get, isPlainObject, merge, set} from 'lodash-es';
import {explodeAssignObject} from './compiler-helpers';

export type UpdateFunction<T> = (document: T) => void;

type UpdateTokenProcessor<T> = (param: any, propPath: PropertyKey[]) => UpdateFunction<T>;

const tokenProcessors: { [token: string]: UpdateTokenProcessor<any> } = {
    $set(param: any, propPath: PropertyKey[]) {
        const entries = explodeAssignObject(param, propPath);
        const needsDeepAssign: boolean[] = [];
        
        for (let i = 0; i < entries.length; i++)
        {
            needsDeepAssign[i] = isPlainObject(entries[i][1]);
        }
        
        return (document: object) => {
            let i = 0;
            for (const [currentPath, value] of entries)
            {
                if (needsDeepAssign[i++])
                    merge(get(document, currentPath), value);
                else
                    set(document, currentPath, value);
            }
        };
    },
    $inc(param: object, propPath: PropertyKey[]) {
        const entries = explodeAssignObject(param, propPath);
        
        return (document: object) => {
            for (const [currentPath, amount] of entries)
            {
                const currentValue = get(document, currentPath, 0);
                
                if (null === currentValue)
                    throw new Error(`Can't increment field with null value. Path: '${currentPath.join('.')}'`);
                
                set(document, currentPath, currentValue + amount);
            }
        };
    }
};

export function compileUpdate<T>(update: object): UpdateFunction<T>
{
    const processors: UpdateFunction<T>[] = [];
    const propPath: PropertyKey[] = [];

    for (const [key, value] of Object.entries(update))
    {
        const tokenProcessor = tokenProcessors[key];

        if (!tokenProcessor)
            throw new Error(`Unknown update operator '${key}'`);

        processors.push(tokenProcessor(value, propPath));
    }
    
    if (!processors.length)
    {
        return () => {};
    }
    else if (1 === processors.length)
    {
        return processors[0];
    }
    else
    {
        return (document: T) => {
            for (let i = 0; i < processors.length; i++)
            {
                processors[i](document);
            }
        };
    }
}