import {get} from 'lodash-es';

export function parsePath(path: PropertyKey): PropertyKey[]
{
    if (typeof path !== 'string')
        return [path];
    
    const segments: PropertyKey[] = path.split('.');
    
    for (let i = 0; i < segments.length; i++)
    {
        const segment = segments[i] as string;
        
        if (!segment)
            throw new Error(`Invalid path '${path}'`);
        
        if (/^\d+$/.test(segment))
            segments[i] = parseInt(segment);
    }
    
    return segments;
}

export function explodeAssignObject(obj: object, basePropPath: PropertyKey[])
{
    const entries = Object.entries(obj); 
    
    return entries.map(
        ([key, value]: [PropertyKey, number]): [PropertyKey[], number] => {
            const newPropPath = [...basePropPath, ...parsePath(key)];
            
            return [newPropPath, value];
        }
    );
}

const textDecoder = new TextDecoder('utf-8');
export function getSortableValue(value: any): any
{
    if (ArrayBuffer.isView(value))
        value = textDecoder.decode(value);
    
    return value;
}

export function compareValues(a: any, b: any): number
{
    a = getSortableValue(a);
    b = getSortableValue(b);

    if (a < b)
        return -1;

    if (b < a)
        return 1;

    return 0;
}

export function sortDocuments<T>(documents: T[], sortOptions: object): T[]
{
    const sorts = explodeAssignObject(sortOptions, []);

    if (sorts.length)
    {
        const sortableValues = documents.map((document): [any[], T] => {
            return [
                sorts.map(
                    currentSort => getSortableValue(get(document, currentSort[0]))
                ),
                document
            ];
        });

        sortableValues.sort(([aValues], [bValues]) => {
            for (let i = 0; i < aValues.length; i++)
            {
                if (aValues[i] < bValues[i])
                {
                    return -1 * sorts[i][1];
                }

                if (bValues[i] < aValues[i])
                {
                    // noinspection PointlessArithmeticExpressionJS
                    return 1 * sorts[i][1];
                }
            }

            return 0;
        });

        return sortableValues.map(entry => entry[1]);
    }
    
    return documents;
}