import {get, isPlainObject, negate} from 'lodash-es';
import {compareValues, parsePath} from './compiler-helpers';

export type QueryFunction<T> = (document: T) => boolean;

type TokenProcessor<T> = (param: any, propPath: PropertyKey[]) => QueryFunction<T>;

const tokenProcessors: { [token: string]: TokenProcessor<any> } = {
    $not(param: object, propPath: PropertyKey[]) {
        if (param instanceof RegExp)
        {
            return document => param.test(get(document, propPath));
        }
        else if (isPlainObject(param))
        {
            return negate(plainObject(param, propPath));
        }
        else
        {
            throw new Error(`Unsupported $not parameter '${param}'`);
        }
    },
    $gt<T>(param: any, propPath: PropertyKey[]) {
        return document => 1 === compareValues(get(document, propPath), param);
    },
    $gte<T>(param: any, propPath: PropertyKey[]) {
        return (document: T) => {
            const result = compareValues(get(document, propPath), param);
            return 1 === result || 0 === result;
        }
    },
    $eq<T>(param: any, propPath: PropertyKey[]) {
        return (document: T) => 0 === compareValues(get(document, propPath), param);
    },
    $neq<T>(param: any, propPath: PropertyKey[]) {
        return (document: T) => 0 !== compareValues(get(document, propPath), param);
    },
    $lte<T>(param: any, propPath: PropertyKey[]) {
        return document => {
            const result = compareValues(get(document, propPath), param);
            return -1 === result || 0 === result;
        };
    },
    $lt<T>(param: any, propPath: PropertyKey[]) {
        return document => -1 === compareValues(get(document, propPath), param);
    },
    $and<T>(param: any[], propPath: PropertyKey[]) {
        const predicates: QueryFunction<T>[] = [];
        
        for (const expr of param)
        {
            predicates.push(plainObject(expr, propPath));
        }

        if (!predicates.length)
        {
            return () => true;
        }
        else if (1 === predicates.length)
        {
            return predicates[0];
        }
        else
        {
            return (document: T) => {
                for (let i = 0; i < predicates.length; i++)
                {
                    if (!predicates[i](document))
                        return false;
                }

                return true;
            };
        }
    },
};

export function plainObject<T>(query: object, propPath: PropertyKey[]): QueryFunction<T>
{
    const predicates: QueryFunction<T>[] = [];

    for (const [key, value] of Object.entries(query))
    {
        let tokenProcessor;
        let isOperator = key.startsWith('$');
        if (isOperator)
        {
            tokenProcessor = tokenProcessors[key];
        }
        else if (isPlainObject(value))
        {
            tokenProcessor = plainObject;
        }
        else
        {
            tokenProcessor = tokenProcessors.$eq;
        }

        if (!tokenProcessor)
            throw new Error(`Operator '${key}' is not supported`);

        // Only append propPath if we entered a sub-property
        const newPropPath = isOperator ? propPath : [...propPath, ...parsePath(key)];

        predicates.push(tokenProcessor(value, newPropPath));
    }

    if (!predicates.length)
    {
        return () => true;
    }
    else if (1 === predicates.length)
    {
        return predicates[0];
    }
    else
    {
        return (document: T) => {
            for (let i = 0; i < predicates.length; i++)
            {
                if (!predicates[i](document))
                    return false;
            }

            return true;
        };
    }
}

export function compileQuery<T>(query: object): QueryFunction<T>
{
    return plainObject(query, []);
}