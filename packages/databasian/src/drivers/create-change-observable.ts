import {Observable} from 'rxjs';
import {filter, startWith} from 'rxjs/operators';
import {properExhaustMap, properThrottle} from '@epicentric/utilbelt';

export function createChangeObservable<T, TResult = T>(observable: Observable<string>, collectionName: string,
                                                       throttle: number|undefined, project: () => Promise<TResult>)
: Observable<TResult>
{
    return observable.pipe(
        filter(changedCollection => changedCollection === collectionName),
        properThrottle(throttle || 0, { trailing: true, leading: true }),
        startWith(null),
        properExhaustMap(project),
    );
}