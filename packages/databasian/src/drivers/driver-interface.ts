import {Observable} from 'rxjs';
import {DocumentInterface} from '../database/database-interface';
import {CollectionConfigs} from '../database/collection-config';

export interface DriverOptions
{
    collectionConfigs?: CollectionConfigs;
    changeThrottle?: number;
}

export type InsertDocument<T> = Partial<T> & DocumentInterface;

export interface DriverInterface
{
    initialize(options: DriverOptions): Promise<void>;
    
    // Querying
    find<T extends DocumentInterface>(collectionName: string, query?: object, sort?: { [key: string]: 1|-1 }, limit?: number): Observable<T[]>;
    findSnapshot<T extends DocumentInterface>(collectionName: string, query?: object, sort?: { [key: string]: 1|-1 }, limit?: number): Promise<T[]>;

    // Updating
    findOneAndUpdate<T extends DocumentInterface>(collectionName: string, filter: object, update: object): Promise<T | null>;
    updateMany<T extends DocumentInterface>(collectionName: string, query: object, update: object): Promise<void>;

    // Inserting
    insertMany<T extends DocumentInterface>(collectionName: string, documents: InsertDocument<T>[]): Promise<T[]>;

    // Removing
    findOneAndRemove<T extends DocumentInterface>(collectionName: string, filter: object): Promise<T|null>;
    removeMany<T extends DocumentInterface>(collectionName: string, filter: object): Promise<T[]>;
}