import {Observable} from 'rxjs';
import {SortOptions} from '../sort-options';
import {ContextType} from '../context-type';
import {DriverInterface, InsertDocument} from '../drivers/driver-interface';
import {BinaryId} from '@epicentric/binary-id';

export interface DocumentInterface
{
    _id: BinaryId;
}

export interface DatabaseInterface extends DriverInterface
{
    // Querying
    findOne<T extends DocumentInterface>(collection: string, query: object, context?: ContextType): Observable<T | null>;
    findOneSnapshot<T extends DocumentInterface>(collection: string, query: object, context?: ContextType): Promise<T | null>;
    find<T extends DocumentInterface>(collection: string, query?: object, sort?: SortOptions, limit?: number, context?: ContextType): Observable<T[]>;
    findById<T extends DocumentInterface>(collection: string, id: BinaryId, context?: ContextType): Observable<T|null>;
    findSnapshot<T extends DocumentInterface>(collection: string, query?: object, sort?: SortOptions, limit?: number, context?: ContextType): Promise<T[]>;

    // Updating
    findByIdAndUpdate<T extends DocumentInterface>(collection: string, id: BinaryId, update: object, context?: ContextType): Promise<T | null>;
    findOneAndUpdate<T extends DocumentInterface>(collection: string, filter: object, update: object, context?: ContextType): Promise<T | null>;
    updateMany<T extends DocumentInterface>(collection: string, query: object, update: object, context?: ContextType): Promise<void>;

    // Inserting
    insertOne<T extends DocumentInterface>(collection: string, document: InsertDocument<T>, context?: ContextType): Promise<T>;

    // Removing
    findByIdAndRemove<T extends DocumentInterface>(collection: string, id: BinaryId, context?: ContextType): Promise<T | null>;
    findOneAndRemove<T extends DocumentInterface>(collection: string, filter: object, context?: ContextType): Promise<T|null>;
    removeMany<T extends DocumentInterface>(collectionName: string, filter: object, context?: ContextType): Promise<T[]>;
}