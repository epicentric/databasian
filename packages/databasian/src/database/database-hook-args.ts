import {ContextType} from '../context-type';
import {SortOptions} from '../sort-options';
import {DatabaseInterface, DocumentInterface} from './database-interface';

const defaultBeforeFetcher = () => Promise.resolve([]);
export class DatabaseHookArgs<T extends DocumentInterface = any>
{
    public get query(): object | undefined
    {
        return this._query;
    }

    public set query(value: object | undefined)
    {
        this._query = value;
        this._queryChanged = true;
    }
    
    public sort?: SortOptions;
    public limit?: number;
    
    public update?: Partial<T>;
    // public before: T[];
    public after?: T[];
    public documents?: T[];
    public beforeFetcher: (this: this) => Promise<T[]> = defaultBeforeFetcher;

    private _query?: object;
    private _queryChanged: boolean = false;
    private _beforeDocs: T[] | null = null;

    public constructor(
        public database: DatabaseInterface,
        public collectionName: string,
        public one: boolean,
        public context?: ContextType,
    )
    {
    }

    public async getBeforeDocs(): Promise<T[]>
    {
        if (this._beforeDocs && !this._queryChanged)
            return Promise.resolve(this._beforeDocs);

        this._queryChanged = false;
        const beforeDocs = await this.beforeFetcher();
        this._beforeDocs = beforeDocs;
        return beforeDocs;
    }
}