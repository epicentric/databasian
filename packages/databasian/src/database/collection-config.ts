import {SortOptions} from '../sort-options';
import {ValidationFunction} from '@epicentric/validational';

export interface CollectionConfig
{
    defaultSort?: SortOptions;
    defaultFields?: { [key: string]: any };
    indexes?: {
        [key: string]: -1 | 1 | { [key: string]: -1 | 1 }
    };
    documentValidator?: ValidationFunction<{ allPropertiesRequired: boolean }>;
}

export type CollectionConfigs = { [collectionName: string]: CollectionConfig };