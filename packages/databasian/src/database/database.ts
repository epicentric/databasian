import {from, Observable, of} from 'rxjs';
import {defaults, set} from 'lodash-es';
import {DriverInterface, DriverOptions, InsertDocument} from '../drivers/driver-interface';
import {DatabaseInterface, DocumentInterface} from './database-interface';
import {HookEmitter, HookOptions} from '@epicentric/hook-emitter';
import {DatabaseHookArgs} from './database-hook-args';
import {CollectionConfig, CollectionConfigs} from './collection-config';
import {SortOptions} from '../sort-options';
import {ContextType} from '../context-type';
import {map, mergeAll} from 'rxjs/operators';
import {properExhaustMap} from '@epicentric/utilbelt';
import {BinaryId} from '@epicentric/binary-id';

export const enum DatabaseHookType
{
    PreInsert,
    PostInsert,
    PreUpdate,
    PostUpdate,
    PreDelete,
    PostDelete,
    PreFind,
    PostFind
}

export interface DatabaseHookOptions extends HookOptions<DatabaseHookType>
{

}

interface DatabaseOptions extends DriverOptions
{
    driver: DriverInterface;
    collectionConfigs?: CollectionConfigs;
    changeThrottle?: number;
}

export class Database implements DatabaseInterface
{
    public get hook()
    {
        return this.hooks.hook;
    }
    
    public readonly ready: Promise<void>;
    public readonly hooks: HookEmitter<DatabaseHookType, DatabaseHookArgs, DatabaseHookOptions>;
    public readonly driver: DriverInterface;
    public readonly collectionConfigs: CollectionConfigs;

    public constructor(options: DatabaseOptions)
    {
        this.driver = options.driver;
        this.collectionConfigs = options.collectionConfigs || {};
        this.hooks = new HookEmitter(Symbol('DATABASE_HOOK'));
        
        this.hooks.register(this.onPreFind.bind(this), {
            type: DatabaseHookType.PreFind
        });
        
        this.hooks.register(this.onPreInsert.bind(this), {
            type: DatabaseHookType.PreInsert
        });
        
        this.ready = this.initialize(options);
    }

    public initialize(options: DriverOptions): Promise<void>
    {
        if (this.ready)
            return this.ready;
        
        return this.driver.initialize(options);
    }

    // region Querying

    public findOne<T extends DocumentInterface>(collectionName: string, query: object, context?: ContextType): Observable<T|null>
    {
        return this.find<T>(collectionName, query, undefined, undefined, context).pipe(
            map(results => results[0] || null)
        );
    }

    public findOneSnapshot<T extends DocumentInterface>(collectionName: string, query: object, context?: ContextType): Promise<T | null>
    {
        return this.findSnapshot<T>(collectionName, query, {}, 1, context).then(docs =>
            docs[0] || null
        );
    }

    public find<T extends DocumentInterface>(collectionName: string, query: object, sort?: SortOptions,
                                                  limit?: number, context?: ContextType): Observable<T[]>
    {
        return from(this.findInner<T>(collectionName, query, sort, limit, context)).pipe(mergeAll());
    }

    public findById<T extends DocumentInterface>(collectionName: string, id: BinaryId, context?: ContextType): Observable<T|null>
    {
        return this.findOne(collectionName, { _id: id }, context);
    }

    public async findSnapshot<T extends DocumentInterface>(collectionName: string, query?: object, sort?: SortOptions, limit?: number, context?: ContextType): Promise<T[]>
    {
        await this.ready;
        
        const hookArgs = new DatabaseHookArgs(this, collectionName, true, context);
        hookArgs.query = query;

        await this.hooks.emit(DatabaseHookType.PreFind, hookArgs);

        if (hookArgs.after)
            return hookArgs.after;

        hookArgs.after = await this.driver.findSnapshot<T>(collectionName, hookArgs.query, hookArgs.sort, hookArgs.limit);

        await this.hooks.emit(DatabaseHookType.PostFind, hookArgs);

        return hookArgs.after;
    }

    // endregion Querying

    // region Updating

    public async findOneAndUpdate<T extends DocumentInterface>(collectionName: string, filter: object, update: object, context?: ContextType): Promise<T|null>
    {
        await this.ready;

        const hookArgs = new DatabaseHookArgs(this, collectionName, true, context);
        hookArgs.query = filter;
        hookArgs.update = update;
        hookArgs.beforeFetcher = function() {
            return this.database.findSnapshot(this.collectionName, this.query!, undefined, 1);
        };

        await this.hooks.emit(DatabaseHookType.PreUpdate, hookArgs);

        if (hookArgs.after)
            return hookArgs.after[0] || null;

        const [beforeDoc] = await hookArgs.getBeforeDocs();

        let afterDoc: T|null = null;
        if (beforeDoc)
        {
            afterDoc = await this.driver.findOneAndUpdate(collectionName, { _id: beforeDoc._id }, hookArgs.update);
        }

        hookArgs.after = afterDoc ? [afterDoc] : [];
        await this.hooks.emit(DatabaseHookType.PostUpdate, hookArgs);

        return hookArgs.after[0] || null;
    }

    public async findByIdAndUpdate<T extends DocumentInterface>(collectionName: string, id: BinaryId, update: object, context?: ContextType): Promise<T|null>
    {
        return this.findOneAndUpdate(collectionName, { _id: id }, update, context);
    }

    public async updateMany<T extends DocumentInterface>(collectionName: string, filter: object, update: object, context?: ContextType): Promise<void>
    {
        await this.ready;

        const hookArgs = new DatabaseHookArgs(this, collectionName, false, context);
        hookArgs.query = filter;
        hookArgs.update = update;
        hookArgs.beforeFetcher = function () {
            return this.database.findSnapshot(this.collectionName, this.query!, { _id: -1  });
        };

        await this.hooks.emit(DatabaseHookType.PreUpdate, hookArgs);

        if (hookArgs.after)
            return;

        const beforeDocs = await hookArgs.getBeforeDocs();
        let afterDocs: T[] = [];
        if (beforeDocs.length)
        {
            const inQuery = {
                _id: {
                    $in: beforeDocs.map(doc => doc._id)
                }
            };
            await this.driver.updateMany(collectionName, inQuery, hookArgs.update);

            afterDocs = await this.findSnapshot<T>(collectionName, inQuery, { _id: -1 });
        }

        hookArgs.after = afterDocs;
        await this.hooks.emit(DatabaseHookType.PostUpdate, hookArgs);
    }

    // endregion

    // region Deleting

    public findByIdAndRemove<T extends DocumentInterface>(collectionName: string, id: BinaryId, context?: ContextType): Promise<T|null>
    {
        return this.findOneAndRemove(collectionName, { _id: id }, context);
    }

    public async findOneAndRemove<T extends DocumentInterface>(collectionName: string, filter: object, context?: ContextType): Promise<T|null>
    {
        await this.ready;

        const hookArgs = new DatabaseHookArgs(this, collectionName, true, context);
        hookArgs.query = filter;
        hookArgs.beforeFetcher = function() {
            return this.database.findSnapshot<T>(this.collectionName, this.query!, undefined, 1);
        };

        await hookArgs.getBeforeDocs();
        await this.hooks.emit(DatabaseHookType.PreDelete, hookArgs);
        const beforeDocs = await hookArgs.getBeforeDocs();

        if (hookArgs.after)
            return beforeDocs[0] || null;

        const result = await this.driver.findOneAndRemove(collectionName, hookArgs.query);
        hookArgs.after = result ? [result] : [];

        await this.hooks.emit(DatabaseHookType.PostDelete, hookArgs);

        return hookArgs.after[0] || null;
    }

    public async removeMany<T extends DocumentInterface>(collectionName: string, filter: object, context?: ContextType): Promise<T[]>
    {
        await this.ready;

        const hookArgs = new DatabaseHookArgs(this, collectionName, true, context);
        hookArgs.query = filter;
        hookArgs.beforeFetcher = function() {
            return this.database.findSnapshot<T>(this.collectionName, this.query!);
        };

        const beforeDocs = await hookArgs.getBeforeDocs();
        await this.hooks.emit(DatabaseHookType.PreDelete, hookArgs);

        if (hookArgs.after)
            return beforeDocs;

        const result = await this.driver.removeMany(collectionName, hookArgs.query);
        hookArgs.after = result;

        await this.hooks.emit(DatabaseHookType.PostDelete, hookArgs);

        return hookArgs.after;
    }

    // endregion Deleting

    public insertOne<T extends DocumentInterface>(collectionName: string, document: InsertDocument<T>, context?: ContextType): Promise<T>
    {
        return this.insertMany(collectionName, [document], context).then(results => results[0]);
    }

    public async insertMany<T extends DocumentInterface>(collectionName: string, documents: InsertDocument<T>[], context?: ContextType): Promise<T[]>
    {
        await this.ready;

        const hookArgs = new DatabaseHookArgs(this, collectionName, false, context);
        hookArgs.documents = documents;

        await this.hooks.emit(DatabaseHookType.PreInsert, hookArgs);

        if (hookArgs.after)
            return hookArgs.after;

        hookArgs.after = await this.driver.insertMany(collectionName, hookArgs.documents!);

        await this.hooks.emit(DatabaseHookType.PostInsert, hookArgs);

        return hookArgs.after;
    }

    private async findInner<T extends DocumentInterface>(collectionName: string, query: object, sort?: SortOptions,
                                                              limit?: number, context?: ContextType): Promise<Observable<T[]>>
    {
        await this.ready;
        
        const hookArgs = new DatabaseHookArgs<T>(this, collectionName, false, context);
        hookArgs.query = query;
        hookArgs.sort = sort;
        hookArgs.limit = limit;

        await this.hooks.emit(DatabaseHookType.PreFind, hookArgs);

        if (hookArgs.after)
            return of(hookArgs.after);

        return this.driver.find(collectionName, hookArgs.query, hookArgs.sort, hookArgs.limit).pipe(
            properExhaustMap(async results => {
                hookArgs.after = results as T[];
                await this.hooks.emit(DatabaseHookType.PostFind, hookArgs);
                return hookArgs.after!;
            })
        );
    }

    public getCollectionConfig(name: string): CollectionConfig
    {
        return this.collectionConfigs[name] || {};
    }
    
    private onPreFind(eventArgs: DatabaseHookArgs)
    {
        if (undefined !== eventArgs.sort)
            return;
        
        const defaultSort = this.getCollectionConfig(eventArgs.collectionName).defaultSort;
        
        if (!defaultSort)
            return;
        
        set(eventArgs, ['options', 'sort'], defaultSort);
    }
    
    private onPreInsert(eventArgs: DatabaseHookArgs, type: DatabaseHookType)
    {
        const defaultFields = this.getCollectionConfig(eventArgs.collectionName).defaultFields;
        
        if (!defaultFields)
            return;

        for (const doc of eventArgs.documents!)
        {
            defaults(doc, defaultFields);
        }
    }
}