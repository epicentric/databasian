import {compileQuery, compileUpdate, sortDocuments} from '@epicentric/databasian';
import {cloneDeep} from 'lodash-es';
import {Subject} from 'rxjs';
import {throttle} from 'rxjs/operators';
import {delay} from '@epicentric/utilbelt';

export interface SimpleCollectionOptions<T>
{
    name: string;
    loadCallback: () => Promise<T[]>;
    saveCallback: (data: T[]) => Promise<void>;
}

export class SimpleCollection<T>
{
    private name: string;
    private loadCallback: () => Promise<T[]>;
    private saveCallback: (data: T[]) => Promise<void>;
    
    private data: T[] = [];
    private loaded: Promise<void>;
    
    private change: Subject<void> = new Subject;
    
    public constructor(options: SimpleCollectionOptions<T>)
    {
        ({
            name: this.name,
            loadCallback: this.loadCallback,
            saveCallback: this.saveCallback
        } = options);
        
        this.loaded = this.initialize();
        
        this.change.pipe(
            throttle(async () => {
                await this.saveCallback(this.data);
                await delay(3000);
            }, { leading: false, trailing: true })
        ).subscribe(() => { });
    }
    
    public async find(query: object, sort?: { [p: string]: 1 | -1 },
                limit: number = Number.POSITIVE_INFINITY): Promise<T[]>
    {
        await this.loaded;
        
        const filterFunc = compileQuery(query);
        let results: T[] = [];
        
        for (let i = 0; i < this.data.length; i++)
        {
            if (filterFunc(this.data[i]))
                results.push(this.data[i]);
        }
        
        if (sort)
            results = sortDocuments(results, sort);
        
        if (Number.isSafeInteger(limit))
            results.length = limit;
        
        return results;
    }
    
    public async remove(query: object): Promise<number>
    {
        await this.loaded;

        const filterFunc = compileQuery(query);
        let deleteCount = 0;
        
        for (let i = 0; i < this.data.length; i++)
        {
            if (!filterFunc(this.data[i]))
                continue;

            // Based on: https://stackoverflow.com/a/28400264
            if (i < this.data.length - 1)
                this.data[i] = this.data[this.data.length - 1];

            this.data.length -= 1;
            deleteCount++;
        }
        
        this.change.next();
        
        return deleteCount;
    }
    
    public async update(query: object, update: object): Promise<number>
    {
        await this.loaded;

        const filterFunc = compileQuery(query);
        const updateFunc = compileUpdate(update);
        let updateCount = 0;
        
        for (let i = 0; i < this.data.length; i++)
        {
            if (!filterFunc(this.data[i]))
                continue;
            
            const newDocument = cloneDeep(this.data[i]);
            updateFunc(newDocument);
            this.data[i] = newDocument;
            
            updateCount++;
        }

        this.change.next();
        
        return updateCount;
    }
    
    public async insert(documents: T[]): Promise<T[]>
    {
        await this.loaded;

        this.data.push(...documents);
        this.change.next();
        
        return documents;
    }
    
    private async initialize()
    {
        this.data = await this.loadCallback();
    }
}