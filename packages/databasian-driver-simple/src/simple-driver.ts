import {DocumentInterface, DriverInterface, DriverOptions, InsertDocument, createChangeObservable} from '@epicentric/databasian';
import {Observable, Subject} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {SimpleCollection} from './simple-collection';
import {BinaryId} from '@epicentric/binary-id';

export interface SimpleDriverOptions
{
    getCollectionCallback: (collectionName: string) => SimpleCollection<any>;
}

export class SimpleDriver implements DriverInterface
{
    private collections: Map<string, SimpleCollection<any>> = new Map;
    private change: Subject<string> = new Subject;
    private getCollectionCallback: (collectionName: string) => SimpleCollection<any>;
    private options!: DriverOptions;
    
    public constructor(options: SimpleDriverOptions)
    {
        ({
            getCollectionCallback: this.getCollectionCallback
        } = options);
    }

    public initialize(options: DriverOptions): Promise<void>
    {
        this.options = options;
        
        return Promise.resolve();
    }
    
    public find<T extends DocumentInterface>(collection: string, query?: object, sort?: { [p: string]: 1 | -1 },
                                             limit?: number): Observable<T[]>
    {
        const dbCollection = this.getCollection<T>(collection);
        
        return createChangeObservable(this.change, collection, this.options.changeThrottle,
            () => dbCollection.find(query || {}, sort, limit));
    }

    public findOne<T extends DocumentInterface>(collection: string, query: object): Observable<T | null>
    {
        return this.find<T>(collection, query, {}, 1).pipe(
            map(results => results[0] || null)
        );
    }

    public findSnapshot<T extends DocumentInterface>(collection: string, query?: object, sort?: { [p: string]: 1 | -1 }, limit?: number): Promise<T[]>
    {
        const dbCollection = this.getCollection<T>(collection);
        
        return dbCollection.find(query || {}, sort, limit);
    }

    public async findOneAndRemove<T extends DocumentInterface>(collection: string, filter: object): Promise<T | null>
    {
        const result = await this.findOnePromise<T>(collection, filter);

        if (!result)
            return null;

        await this.getCollection<T>(collection).remove({ _id: result._id });
        this.change.next(collection);

        return result;
    }

    public async removeMany<T extends DocumentInterface>(collectionName: string, filter: object): Promise<T[]>
    {
        const result = await this.findSnapshot<T>(collectionName, filter);

        if (!result.length)
            return result;

        await this.getCollection<T>(collectionName).remove(filter);
        this.change.next(collectionName);

        return result;
    }

    public async findOneAndUpdate<T extends DocumentInterface>(collection: string, filter: object, update: object): Promise<T | null>
    {
        const result = await this.findOnePromise<T>(collection, filter);
        
        if (!result)
            return null;
        
        return this.findByIdAndUpdate<T>(collection, result._id, update);
    }

    public async findByIdAndUpdate<T extends DocumentInterface>(collection: string, id: BinaryId, update: object): Promise<T | null>
    {
        const query = { _id: id };
        
        const updateCount = await this.getCollection<T>(collection).update(query, update);
        
        if (!updateCount)
            return null;

        this.change.next(collection);
        
        return await this.findOnePromise(collection, query);
    }

    public async updateMany<T extends DocumentInterface>(collection: string, query: object, update: object): Promise<void>
    {
        await this.getCollection<T>(collection).update(query, update);
        this.change.next(collection);
    }

    public async insertMany<T extends DocumentInterface>(collection: string, documents: InsertDocument<T>[]): Promise<T[]>
    {
        const results = await this.getCollection<T>(collection).insert(documents);
        this.change.next(collection);
        return results;
    }
    
    private getCollection<T>(name: string)
    {
        let collection = this.collections.get(name);
        
        if (!collection)
        {
            collection = this.getCollectionCallback(name);
            this.collections.set(name, collection);
        }
        
        return collection;
    }
    
    private findOnePromise<T extends DocumentInterface>(collection: string, query: object): Promise<T|null>
    {
        return this.findOne<T>(collection, query).pipe(take(1)).toPromise()
    }
}